Quickstart
==========


1. Clone this `git clone https://bitbucket.org/mvigariu/searcher.git`
2. Create a virtual environment and activate it.
3. Change directory to root of the project (where `setup.py` file is)
4. Run `python setup.py develop`


Run
===

1. `python searcher/run.py --path=/some/path/to/txt/files`

`path` is optional and if not provided will default to `fixtures` folder
