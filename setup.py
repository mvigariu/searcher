import setuptools


with open("README.md", "r") as f:
    long_descr = f.read()


setuptools.setup(
    name='searcher',
    version='0.0.1',
    author='Marius Vigariu',
    author_email='marius.vigariu@gmail.com',
    description='Basic search fun project',
    long_description=long_descr,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
)
