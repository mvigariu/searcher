import unittest
import os

from searcher.core import Index


class TestIndex(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.index = Index(os.path.join(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))), 'fixtures'))

    def test_index_build(self):
        self.index.build()
        self.assertEqual(len(self.index._data.keys()), 1155)

    def test_search(self):
        results = self.index.search('two sentiments')
        self.assertEqual(len(results), 3)


if __name__ == '__main__':
    unittest.main()
