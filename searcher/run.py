import argparse
import cmd
import os
import time

from searcher.core import Index


class Console(cmd.Cmd):
    prompt = 'search> '

    def __init__(self, index):
        cmd.Cmd.__init__(self)
        self.index = index

    def do_EOF(self, args):
        """ Exits on CTRL+D """
        return -1

    def do__index_rebuild(self, *a):
        start = time.time()
        print('Rebuilding index ...')
        self.index.clear()
        self.index.build()
        delta = round((time.time() - start), 2)
        print(f"Index rebuilt in {delta} seconds")

    def postloop(self):
        print("Exiting...")

    def emptyline(self):
        """ Do nothing on empty input line """
        pass

    def default(self, line):
        results = index.search(line)

        if not results:
            print('no matches found')
        else:
            for f, score in results:
                score = round(score * 100, 2)
                if score > 100:
                    score = 100

                print(f'{f}: {score}%')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', action='store', dest='path', type=str)

    args = parser.parse_args()

    path = args.path or os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        'fixtures'
    )

    assert os.path.isdir(path)

    start = time.time()
    print('Building index ...')
    index = Index(path)
    index.build()

    delta = round((time.time() - start), 2)
    print(f'Index built in {delta} seconds')

    console = Console(index)

    try:
        console.cmdloop()
    except KeyboardInterrupt:
        print('Exiting...')
