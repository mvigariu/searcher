import os

from collections import defaultdict


class Index(object):
    def __init__(self, path, ext=None):
        self.path = path
        assert os.path.isdir(self.path)

        self.ext = ext or '.txt'

    _all_files = None
    _data = None
    _data_bytes = None

    @property
    def all_files(self):
        """ Recursively scans all text files and returns a list of them.
        """
        if self._all_files is not None:
            return self._all_files

        data = []

        for path, dirs, files in os.walk(self.path):
            for f in files:
                if f.endswith(self.ext):
                    data.append(os.path.join(path, f))
        return data

    def clear(self):
        """ Clears the index data.
        """
        self._all_files = None
        self._data = None
        self._data_bytes = None

    def build(self):
        """ Builds index data as a dictionary mapping between a processed word
        and the source and how many times it was found.

        Eg: {word: {file: 3 times}, ...}
        """
        if self._data is not None:
            return self._data

        data = defaultdict(lambda: defaultdict(int))

        self._data_bytes = {}

        for one in self.all_files:

            self._data_bytes[one] = os.stat(one).st_size

            with open(one) as f:
                for line in f:
                    if not line.strip():
                        continue

                    for word in self.tokenize(line):
                        data[word][one] += 1

        self._data = data
        return self._data

    def tokenize(self, line):
        """ Dummy tokenizer method.

        Can be extended to throw the line through a series of pipelines.
        """
        return line.lower().split()

    def analyze(self, query):
        """ Dummy analyzer method.

        Can be extended to throw the query through a series of pipelines.
        """
        return list(map(str.lower, query.split()))

    def search(self, query):
        """ Analyzes query terms and performs the search.

        Returns a two item tuples list sorted by rank.

        Eg: [(file, rank), ...]
        """
        terms = self.analyze(query)

        results = defaultdict(float)

        def weigh(term, count, f):
            """ Gives a weight to ``term``

            Takes into account how many terms are being, how many times the
            ``term`` shows up in ``f`` and the size of ``f``
            """

            exists = 1.0 / len(terms)
            repeats = count * 10.0 / self._data_bytes[f]

            return exists + repeats

        for term in terms:
            data = self._data.get(term)
            if data is None:
                continue

            for f, count in data.items():
                results[f] += weigh(term, count, f)

        return sorted(results.items(), key=lambda item: item[1], reverse=True)
